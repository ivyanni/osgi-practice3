package ru.tersoft.scr.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import ru.tersoft.scr.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of service which prints "hello world" message.
 * Uses SCR annotations for service registration.
 */
@Component(name = "Greeting")
@Service(Greeting.class)
public class GreetingImpl implements Greeting {
    public void sayHello() {
        System.out.println("Hello OSGi World!");
    }
}
