package ru.tersoft.scr;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service which prints "Hello world" message.
 */
public interface Greeting {
    void sayHello();
}
