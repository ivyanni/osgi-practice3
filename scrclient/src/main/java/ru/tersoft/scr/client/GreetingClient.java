package ru.tersoft.scr.client;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import ru.tersoft.scr.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * Consumer of Hello World service for Apache Felix.
 * Uses SCR annotations for binding and activation.
 */
@Component(name = "GreetingClient", immediate = true)
public class GreetingClient {
    @Reference(
            name = "greeting",
            referenceInterface = Greeting.class,
            bind = "setGreeting",
            unbind = "unsetGreeting"
    )
    private Greeting greeting;

    @Activate
    protected void start() {
        greeting.sayHello();
    }

    protected void setGreeting(Greeting greeting) {
        this.greeting = greeting;
    }

    protected void unsetGreeting(Greeting greeting) {
        this.greeting = null;
    }
}
